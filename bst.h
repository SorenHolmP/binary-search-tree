#ifndef BST_H
#define BST_H

#include <vector>

struct TreeNode
{
    TreeNode(int v): val(v){}

    TreeNode* left;
    TreeNode* right;
    int val;
};

class BST
{
public:
    BST(TreeNode* root);
    TreeNode* insert(int val);
    TreeNode* search(int val);
    ~BST();

private:
    TreeNode* insert(TreeNode* n, int val);
    TreeNode* search(TreeNode* n, int val);
private:
    TreeNode* root_;
    std::vector<TreeNode*> nodes;
};

#endif // BST_H
