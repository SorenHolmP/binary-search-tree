#include "bst.h"


BST::BST(TreeNode* root)
:  root_(root)
{

}

TreeNode* BST::insert(int val)
{
    return insert(root_, val);
}


TreeNode* BST::search(int val)
{
    return search(root_, val);
}

BST::~BST()
{
    for(TreeNode* node : nodes)
        delete node;
}



TreeNode* BST::insert(TreeNode* n, int val)
{
    if(val < n->val)
    {
        if(n->left)
            insert(n->left, val);
        else
        {
            TreeNode* nn = new TreeNode(val);
            n->left = nn;
            nodes.push_back(nn);
            return nn;
        }
    }
    else if(val > n->val)
    {
        if(n->right)
            insert(n->right, val);
        else
        {
            TreeNode* nn = new TreeNode(val);
            n->right = nn;
            nodes.push_back(nn);
            return nn;
        }
    }
    else //The value is already in the tree
        return NULL;
}

TreeNode* BST::search(TreeNode* n, int val)
{
    if(n->val == val)
        return n;
    else if(val < n->val)
    {
        if(n->left)
            return search(n->left, val);
        else
            return NULL;
    }
    else if(val > n->val)
        if(n->right)
            return search(n->right, val);
        else
            return NULL;
}
